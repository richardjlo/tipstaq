//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Richard Lo on 8/21/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
