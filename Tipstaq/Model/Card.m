//
//  Card.m
//  Matchismo
//
//  Created by Richard Lo on 8/21/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "Card.h"

@interface Card()

@end

@implementation Card

-(int)match:(NSArray *)otherCards
{
    int score=0;
    
    for (Card* card in otherCards) {
        if ([card.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }
    return score;
}

- (NSString *)description
{
    return self.contents;
}


@end
