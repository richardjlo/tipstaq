//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Richard Lo on 8/27/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

// designated initializer 
- (id)initWithCardCount:(NSUInteger)cardCount
              usingDeck:(Deck *)deck;

- (Card *)cardAtIndex:(NSUInteger)index;

- (void)flipCardAtIndex:(NSUInteger)index;

@property (nonatomic, readonly) int totalScore;
@property (nonatomic) int score;
@property (strong, nonatomic, readonly) Card *card1;
@property (strong, nonatomic, readonly) Card *card2;
@property (nonatomic, readonly) BOOL noMatch;

@end
