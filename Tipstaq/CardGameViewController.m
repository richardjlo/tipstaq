
//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Richard Lo on 8/20/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@end

@implementation CardGameViewController

- (CardMatchingGame *)game
{
    if(!_game) _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                         usingDeck:[[PlayingCardDeck alloc] init]];
    return _game;
}

- (void)setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    [self updateUI];
}

- (void)updateUI
{
    for (UIButton *cardButton in self.cardButtons) {
        Card *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]]; //maps between model and UI
        [cardButton setTitle:card.contents forState:UIControlStateSelected]; //sets title of front of card
        [cardButton setTitle:card.contents forState:UIControlStateSelected|UIControlStateDisabled]; //sets label when it's selected and disabled at the same time
        cardButton.selected = card.isFaceUp; //if it's selected, then it's faceUp
        cardButton.enabled = !card.isUnplayable; //it's playable
        cardButton.alpha = card.isUnplayable ? 0.3 : 1.0; //UI elements have a property called alpha (transparency). 1.0 opaque, 0.0 transparent
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.totalScore];
    if (self.game.score) {
        if (self.game.score > 0) {
            self.resultLabel.text = [NSString stringWithFormat:@"Matched %@ & %@ for %d points!", self.game.card1.contents, self.game.card2.contents, self.game.score];
        } else {
            self.resultLabel.text = [NSString stringWithFormat:@"%@ and %@ don't match! %d point penalty!", self.game.card1.contents, self.game.card2.contents, self.game.score * -1];
        }
        
    }
    self.game.score = 0;
    
    if (self.game.noMatch) {
        self.resultLabel.text = [NSString stringWithFormat:@"Flipped up %@", self.game.card1.contents];
    }
}


-(void)setFlipCount:(int)flipCount
{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips:%d", self.flipCount];
}

/* Method: flipCard
 * @params: (UIButton *sender)
 * pull random card from deck. We don't need to
 * initailize deck because we set Getter above using lazy instantiation
 */
- (IBAction)flipCard:(UIButton *)sender
{
    [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    self.flipCount++;
    [self updateUI];
}

@end
