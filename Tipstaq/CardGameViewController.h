//
//  CardGameViewController.h
//  Matchismo
//
//  Created by Richard Lo on 8/20/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCardDeck.h"

@interface CardGameViewController : UIViewController
@end
